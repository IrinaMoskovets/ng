angular.module('BooksAppServices', ['ngResource']).
    factory('Books', function($resource){
  return $resource('data/books.json', {}, {
    query: {method:'GET', isArray:true}
  });
});


angular.module('BookAppServices', ['ngResource']).
    factory('Book', function($resource){
  return $resource('data/book.json', {}, {
    get: {method:'GET'}
  });
});
